package controllers;

import animations.Shake;
import api.RestRequest;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;


import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class AuthController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private AnchorPane authPane;

    @FXML
    private TextField login_field;

    @FXML
    private PasswordField password_field;

    @FXML
    private Button auth_button;

    @FXML
    private Button signUp_button;

    @FXML
    private AnchorPane signUpPane;

    @FXML
    private TextField signUp_login_field;

    @FXML
    private PasswordField signUp_password_field;

    @FXML
    private Button toSignUp_button;

    @FXML
    private TextField email_field;

    @FXML
    private Button backToAuth_button;

    private String ok = "1";
    private String success = null;
    private StringBuffer authRequest = null;
    private JSONObject loginRequest = null;

    @FXML
    void initialize() {

        auth_button.setOnAction(event ->{
            String loginText = login_field.getText().trim();
            String loginPass = password_field.getText().trim();

            if (!loginText.isEmpty() && !loginPass.isEmpty()) {
                try {
                    authRequest = RestRequest.authRequest(RestRequest.AUTH, loginText, loginPass);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                loginRequest = new JSONObject(authRequest.toString());
                success = loginRequest.get("success").toString();

                if (success.equals(ok)) {
                    System.out.println("true");
                    Main.setToken(loginRequest.get("token_auth").toString());
                    Main.setUserID(loginRequest.get("user_id").toString());
                    Main.setUserLogin(login_field.getText());

                    openNewScene("/fxmlFiles/menu.fxml");
                    Main.isStarted = 1;
                }
                else {
                    Shake userLoginAnim = new Shake(login_field);
                    Shake userPassAnim = new Shake(password_field);
                    userLoginAnim.playAnim();
                    userPassAnim.playAnim();
                }  }else System.out.println("Логин и Пароль пусты.");
        });
        signUp_button.setOnAction(event -> { changePane(authPane, signUpPane); });

        backToAuth_button.setOnAction(event -> { changePane(signUpPane, authPane); });
    }

    public void openNewScene(String window) {
        auth_button.getScene().getWindow().hide();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource(window));
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Parent root = loader.getRoot();
        Stage stage = new Stage();
        stage.setResizable(false);
        stage.setScene(new Scene(root));
        stage.showAndWait();
    }
    public void changePane(AnchorPane from, AnchorPane to) {
        from.setVisible(false);
        from.setManaged(false);

        to.setVisible(true);
        to.setManaged(true);
    }
}
