package controllers;

import java.awt.*;
import java.net.URL;
import java.util.*;
import java.util.List;

import api.RestRequest;
import data.Persons;
import data.Sites;
import data.TotalStatPersons;
import data.TotalStatSites;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.Chart;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.PieChart;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;


import org.json.JSONArray;
import org.json.JSONObject;

public class TotalStatController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TabPane totalStatTabPane;

    @FXML
    private Tab sitesTab;

    @FXML
    private AnchorPane sitesPane;

    @FXML
    private ComboBox<Sites> siteCombo;

    @FXML
    private Label siteLabel;

    @FXML
    private TableView<TotalStatSites> totalStatSitesTable;

    @FXML
    private TableColumn<TotalStatSites, String> personIdColumn;

    @FXML
    private TableColumn<TotalStatSites, String> personNameColumn;

    @FXML
    private TableColumn<TotalStatSites, String> personMentoinsColumn;

    @FXML
    private PieChart sitesPieChart;

    @FXML
    private Tab personsTab;

    @FXML
    private AnchorPane personsPane;

    @FXML
    private Label personLabel;

    @FXML
    private TableView<TotalStatPersons> statPersTable;

    @FXML
    private TableColumn<TotalStatPersons, String> siteIdColumn;

    @FXML
    private TableColumn<TotalStatPersons, String> siteNameColumn;

    @FXML
    private TableColumn<TotalStatPersons, String> siteMentionsColumn;

    @FXML
    private ComboBox<Persons> personCombo;

    @FXML
    private PieChart personsPieChart;

    @FXML
    private Label dateNowLabel;

    @FXML
    private Label dateNowLabel2;

    private String req = null;
    private String request = null;
    private StringBuffer rq = null;
    JSONArray statPersArray = null;

    ObservableList<TotalStatPersons> statPersData = FXCollections.observableArrayList();
    ObservableList<TotalStatSites> statSitesData = FXCollections.observableArrayList();
    ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList();
    ObservableList<PieChart.Data> pieChartData2 = FXCollections.observableArrayList();


    KeywordsController kwContr = new KeywordsController();
    SitesController sitesContr = new SitesController();
    Date date=Calendar.getInstance().getTime();



    @FXML
    void initialize() {
        siteIdColumn.setCellValueFactory(new PropertyValueFactory<>("siteId"));
        siteNameColumn.setCellValueFactory(new PropertyValueFactory<>("siteName"));
        siteMentionsColumn.setCellValueFactory(new PropertyValueFactory<>("PersonRank"));

        personIdColumn.setCellValueFactory(new PropertyValueFactory<>("personId"));
        personNameColumn.setCellValueFactory(new PropertyValueFactory<>("personName"));
        personMentoinsColumn.setCellValueFactory(new PropertyValueFactory<>("persRank"));

        kwContr.loadData();
        sitesContr.loadData();

        personCombo.setItems(kwContr.personsData);

        siteCombo.setItems(sitesContr.sitesData);

        personCombo.setCellFactory(p -> new ListCell<Persons>() {

            @Override
            protected void updateItem(Persons item, boolean empty) {
                super.updateItem(item, empty);
                if (item != null && !empty) {
                    setText(item.getName());
                } else {
                    setText(null);
                }
            }
        });
        personCombo.setButtonCell(new ListCell<Persons>() {
            @Override
            protected void updateItem(Persons item, boolean empty) {
                super.updateItem(item, empty);
                if (item != null && !empty) {
                    setText(item.getName());
                    personLabel.setText(item.getName());
                    refreshPersons(item.getId());
                    dateNowLabel2.setText("Данные на: "+ date);
                } else {
                    setText(null);
                }
            }
        });
        siteCombo.setCellFactory(p -> new ListCell<Sites>() {

            @Override
            protected void updateItem(Sites item, boolean empty) {
                super.updateItem(item, empty);
                if (item != null && !empty) {
                    setText(item.getName());
                } else {
                    setText(null);
                }
            }
        });
        siteCombo.setButtonCell(new ListCell<Sites>() {
            @Override
            protected void updateItem(Sites item, boolean empty) {
                super.updateItem(item, empty);
                if (item != null && !empty) {
                    setText(item.getName());
                    siteLabel.setText(item.getName());
                    refreshSites(item.getName());
                    dateNowLabel.setText("Данные на: "+ date);
                } else {
                    setText(null);
                }
            }
        });
    }
    public void loadStatPersons(String persId) {
        try {
            request = RestRequest.methGetToTal("20130101","20180710", "_personids",persId, "year");
            statPersArray = new JSONArray(request.toString());
            for (int i = 0; i < statPersArray.length(); i++) {
                JSONObject arr = statPersArray.getJSONObject(i);
                String id = arr.get("site_id").toString();
                String name = arr.get("site_name").toString();
                String rank = arr.get("person_rank").toString();
                Double rank2 = Double.parseDouble(rank);
                statPersData.add(new TotalStatPersons(id,name,rank));
                pieChartData2.add(new PieChart.Data(name,rank2));
            } statPersTable.setItems(statPersData);
            personsPieChart.setData(pieChartData2);
            personsPieChart.setTitle("Статистика по личности: "+ personCombo.getValue().getName());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void loadStatSites(String siteName) {
        try {
            req = RestRequest.methGetToTal("20130101","20180710", "_site",siteName, "year");
            statPersArray = new JSONArray(req.toString());
            for (int i = 0; i < statPersArray.length(); i++) {
                JSONObject arr = statPersArray.getJSONObject(i);
                String id = arr.get("person_id").toString();
                String name = arr.get("person_name").toString();
                String rank = arr.get("person_rank").toString();
                Double rank2 = Double.parseDouble(rank);
                statSitesData.add(new TotalStatSites(id, name, rank));
                pieChartData.add(new PieChart.Data(name,rank2));
            } totalStatSitesTable.setItems(statSitesData);
            sitesPieChart.setData(pieChartData);
          sitesPieChart.setTitle("Статистика по сайту "+ siteCombo.getValue().getName());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void refreshPersons(String id) {
        statPersData.clear();
        loadStatPersons(id);
    }
    public void refreshSites(String name) {
        statSitesData.clear();
        loadStatSites(name);
    }


}
