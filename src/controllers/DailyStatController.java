package controllers;


import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

import api.RestRequest;
import data.DailyStatPersonsSites;
import data.Persons;
import data.Sites;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.PieChart;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.StringConverter;
import org.json.JSONArray;
import org.json.JSONObject;

public class DailyStatController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private ComboBox<Sites> siteCombo;

    @FXML
    private ComboBox<Persons> personCombo;

    @FXML
    private DatePicker datePickerFrom;

    @FXML
    private DatePicker datePickerTill;

    @FXML
    private TableView<DailyStatPersonsSites> dailyStatTable;

    @FXML
    private TableColumn<DailyStatPersonsSites, String> dateColumn;

    @FXML
    private TableColumn<DailyStatPersonsSites, String> newPagesColumn;

    @FXML
    private PieChart dailyPieChart;

    @FXML
    private Button enter_button;

    @FXML
    private Label siteLabel;

    @FXML
    private Label personlabel;


    private String rq = null;
    JSONArray statArray = null;
    ObservableList<DailyStatPersonsSites> statData = FXCollections.observableArrayList();
    ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList();

    KeywordsController kwContr = new KeywordsController();
    SitesController sitesContr = new SitesController();

    @FXML
    void initialize() {
        dateColumn.setCellValueFactory(new PropertyValueFactory<>("rankDate"));
        newPagesColumn.setCellValueFactory(new PropertyValueFactory<>("personRank"));
        setCell(siteCombo, personCombo);

        kwContr.loadData();
        sitesContr.loadData();

        personCombo.setItems(kwContr.personsData);
        personCombo.setPromptText("Выберите персону");

        siteCombo.setItems(sitesContr.sitesData);
        siteCombo.setPromptText("Выберите сайт");

        dailyStatTable.setItems(statData);

        datePickerFrom.setValue(LocalDate.now());
        datePickerTill.setValue(LocalDate.now());

        enter_button.setOnAction(event -> {
            convertDate(datePickerFrom);
            convertDate(datePickerTill);
            refreshDailyStat(personCombo.getValue().getId(), siteCombo.getValue().getName(), ((TextField)datePickerFrom.getEditor()).getText(),
                    ((TextField)datePickerTill.getEditor()).getText());
            siteLabel.setText(siteCombo.getValue().getName());
            personlabel.setText(personCombo.getValue().getName());
        });
    }

    public void convertDate(DatePicker datePicker) {
        datePicker.setConverter(new StringConverter<LocalDate>() {
            String pattern = "yyyyMMdd";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);
            { datePicker.setPromptText(pattern.toLowerCase()); }

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });
    }
    public void loadStat(String persId, String siteName, String dateFrom, String dateTill) {
        try {
            rq = RestRequest.methGetDaily(dateFrom, dateTill, persId, siteName, "day");
            statArray = new JSONArray(rq.toString());
            for (int i = 0; i < statArray.length(); i++) {
                JSONObject arr = statArray.getJSONObject(i);
                String rank = arr.get("person_rank").toString();
                String date = arr.get("person_rank_date").toString();
                Double rank2 = Double.parseDouble(rank);
                statData.add(new DailyStatPersonsSites(date,rank));
                pieChartData.add(new PieChart.Data(date,rank2));
            } dailyStatTable.setItems(statData);
            dailyPieChart.setData(pieChartData);
            dailyPieChart.setTitle("Статистика за указанный период");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void refreshDailyStat(String persId, String siteName, String dateFrom, String dateTill){
        statData.clear();
        pieChartData.clear();
        loadStat(persId, siteName, dateFrom, dateTill);

    }
    public void setCell(ComboBox comboBox, ComboBox comboBox2) {
        comboBox.setCellFactory(p -> new ListCell<Sites>() {

            @Override
            protected void updateItem(Sites item, boolean empty) {
                super.updateItem(item, empty);
                if (item != null && !empty) {
                    setText(item.getName());
                } else {
                    setText(null);
                }
            }
        });
        comboBox.setButtonCell(new ListCell<Sites>() {
            @Override
            protected void updateItem(Sites item, boolean empty) {
                super.updateItem(item, empty);
                if (item != null && !empty) {
                    setText(item.getName());
                } else {
                    setText(null);
                }
            }
        });
        comboBox2.setCellFactory(p -> new ListCell<Persons>() {

            @Override
            protected void updateItem(Persons item, boolean empty) {
                super.updateItem(item, empty);
                if (item != null && !empty) {
                    setText(item.getName());
                } else {
                    setText(null);
                }
            }
        });
        comboBox2.setButtonCell(new ListCell<Persons>() {
            @Override
            protected void updateItem(Persons item, boolean empty) {
                super.updateItem(item, empty);
                if (item != null && !empty) {
                    setText(item.getName());
                } else {
                    setText(null);
                }
            }
        });
    }
}
