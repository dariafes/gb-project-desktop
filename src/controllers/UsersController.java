package controllers;

import api.RestRequest;
import data.Logs;
import data.Users;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import javafx.scene.control.cell.PropertyValueFactory;
import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;
import java.util.ResourceBundle;

public class UsersController {
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TableView<Users> usersTable;
    @FXML
    private TableColumn<Users, String> columnAddBy;

    @FXML
    private TableColumn<Users, String> columnEmail;

    @FXML
    private TableColumn<Users, String> columnId;

    @FXML
    private TableColumn<Users, String> columnIsAdmin;

    @FXML
    private TableColumn<Users, String> columnLogin;

    @FXML
    private Button loadCSV_button;

    @FXML
    private Button delete_button;

    @FXML
    private Button editUser_button;

    @FXML
    private Button clearSelection_button;

    @FXML
    private Button add_button;

    @FXML
    private TextField txt_Id;

    @FXML
    private TextField txt_login;

    @FXML
    private PasswordField txt_pass;

    @FXML
    private TextField txt_email;

    @FXML
    private TextField search_field;

    @FXML
    private RadioButton isAdmin1Button;

    @FXML
    private RadioButton isAdmin0Button;

    private StringBuffer getRequest = null;
    private StringBuffer deleteRequest = null;
    private StringBuffer postRequest = null;
    private String patchRequest = null;
    private String postReq = null;
    private String deleteReq = null;
    JSONArray usersArray = null;
//    MenuController menuContr = new MenuController();
    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());

    Date date=Calendar.getInstance().getTime();


    ObservableList<Users> usersData = FXCollections.observableArrayList();

    @FXML
    void initialize() {
        columnEmail.setCellValueFactory(new PropertyValueFactory<>("email"));
        columnId.setCellValueFactory(new PropertyValueFactory<>("id"));
        columnIsAdmin.setCellValueFactory(new PropertyValueFactory<>("isadmin"));
        columnLogin.setCellValueFactory(new PropertyValueFactory<>("login"));
        loadData();
        usersTable.setItems(usersData);
        usersTable.setOnMouseClicked(event -> {
            selectRow();
        });
        clearSelection_button.setOnAction(event -> {
            usersTable.getSelectionModel().clearSelection();
            clearFields();
        });
        add_button.setOnAction(event -> {
            if (validateFields()) {
                addNewUser();
                refreshTable();
//                logsData.add(new Logs(Main.getUserLogin(),"add new user",date.toString()));
//                logTable.setItems(logsData);
//                loadLogs(Main.getUserLogin(), "добавил нового пользователя",timeStamp);
            }
        });
        delete_button.setOnAction(event -> {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Удаление");
            alert.setHeaderText(null);
            alert.setContentText("Вы уверены, что хотите удалить пользователя " + txt_login.getText() + " ?");
            alert.getDialogPane().setStyle("-fx-background-color:" +
                    " linear-gradient(from 0% 0% to 100% 80%, #45BCCD 0%, #9AD9E2 250%);");
            Optional <ButtonType> action = alert.showAndWait();
            if (action.get() == ButtonType.OK) {
                deleteUser();
                refreshTable();
            }
        });
        editUser_button.setOnAction(event -> {
            if (errorOfPatching()) {
                patchUser();
                refreshTable();
            }
        });
        isAdmin1Button.setOnAction(event -> {
            isAdmin1Button.setSelected(true);
            isAdmin0Button.setSelected(false);
        });
        isAdmin0Button.setOnAction(event -> {
            isAdmin1Button.setSelected(true);
            isAdmin1Button.setSelected(false);
        });
    }

    public void loadData() {
        try {
            String myToken = URLEncoder.encode(Main.getToken());
            getRequest = RestRequest.sendGet(RestRequest.USERS+ myToken,"GET");
            usersArray = new JSONArray(getRequest.toString());
            for (int i = 0; i < usersArray.length(); i++) {
                JSONObject user = usersArray.getJSONObject(i);
                String addby = user.get("user_addby").toString();
                String email = user.get("user_email").toString();
                String id = user.get("user_id").toString();
                String isadmin = user.get("user_isadmin").toString();
                String login = user.get("user_login").toString();
                String password = user.get("user_password").toString();
                usersData.add(new Users(addby,email,id,isadmin,login,password));
            } usersTable.setItems(usersData);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String createNewUser(){
        JSONObject newUser = new JSONObject();
        newUser.put("token_auth",Main.getToken());
        newUser.put("user",txt_login.getText());
        newUser.put("password",txt_pass.getText());
        newUser.put("email",txt_email.getText());
        if (isAdmin1Button.isSelected()) {
            newUser.put("isAdmin","1");
            isAdmin0Button.setSelected(false);
        } else if (isAdmin0Button.isSelected()) {
            newUser.put("isAdmin","0");
            isAdmin1Button.setSelected(false);
        }
        String json = newUser.toString();
        return json;
    }
    public String selectUserToDelete(){
        JSONObject selectedUser = new JSONObject();
        selectedUser.put("token_auth",Main.getToken());
        selectedUser.put("user_id",txt_Id.getText());
        String obj = selectedUser.toString();
        return obj;
    }
    public String selectUserToPatch(){
            JSONObject patchUser = new JSONObject();
            patchUser.put("token_auth",Main.getToken());
            patchUser.put("user_id",txt_Id.getText());
                patchUser.put("user",txt_login.getText());
                patchUser.put("email",txt_email.getText());
                patchUser.put("password",txt_pass.getText());
        if (isAdmin1Button.isSelected()) {
            patchUser.put("isAdmin","1");
        } else if (isAdmin0Button.isSelected()) {
            patchUser.put("isAdmin","0");
        }
            String patched = patchUser.toString();
            return patched;
    }

    public void addNewUser(){
                try {
            postReq = RestRequest.methPost(RestRequest.USERS, createNewUser());
            clearFields();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void deleteUser(){
        try {
            deleteRequest = RestRequest.sendPost(RestRequest.USERS,
                    selectUserToDelete(),"DELETE");
//            deleteReq = RestRequest.methDelete("/users","user_id",txt_Id.getText());

            clearFields();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void patchUser(){
        try {
            patchRequest = RestRequest.methPatch(RestRequest.USERS_WITHOUT_TOKEN,
                    selectUserToPatch());
            clearFields();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void selectRow(){
        Users user = usersTable.getSelectionModel().getSelectedItem();
        txt_Id.setText(user.getId());
        txt_login.setText(user.getLogin());
        txt_email.setText(user.getEmail());
        txt_pass.setText(user.getPassword());
        if (user.getIsadmin().equals("1")) {
            isAdmin1Button.setSelected(true);
            isAdmin0Button.setSelected(false);
        } else if (user.getIsadmin().equals("0")) {
            isAdmin0Button.setSelected(true);
            isAdmin1Button.setSelected(false);
        } else {
            isAdmin1Button.setSelected(false);
            isAdmin0Button.setSelected(false);
        }
    }

    public void clearFields() {
        txt_login.clear();
        txt_pass.clear();
        txt_email.clear();
        txt_Id.clear();
        isAdmin1Button.setSelected(false);
        isAdmin0Button.setSelected(false);
    }

    public void refreshTable() {
        usersData.clear();
        loadData();
        usersTable.setItems(usersData);
    }
    private boolean validateFields() {
        if (txt_login.getText().isEmpty() | txt_pass.getText().isEmpty() | txt_email.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Пустые Поля");
            alert.setHeaderText(null);
            alert.setContentText("Пожалуйста, заполните все доступные пустые поля.");
            alert.getDialogPane().setStyle("-fx-background-color:" +
                    " linear-gradient(from 0% 0% to 100% 80%, #45BCCD 0%, #9AD9E2 250%);");
            alert.showAndWait();

            return false;
        }
        return true;
    }
    private boolean errorOfPatching() {
        if (txt_login.getText().isEmpty() | txt_email.getText().isEmpty()){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Ошибка Редактирования");
            alert.setHeaderText(null);
            alert.setContentText("Все доступные поля должны быть заполнены.");
            alert.getDialogPane().setStyle("-fx-background-color:" +
                    " linear-gradient(from 0% 0% to 100% 80%, #45BCCD 0%, #9AD9E2 250%);");
            alert.showAndWait();

            return false;
        }
        return true;
    }
}
