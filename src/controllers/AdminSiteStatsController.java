package controllers;

import api.RestRequest;
import data.Pages;
import data.Sites;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.Date;

public class AdminSiteStatsController extends SitesController {

    @FXML
    private AnchorPane siteCheckBox;

    @FXML
    private Label admLabel;

    @FXML
    private Label siteLabel;

    @FXML
    private TableView<Pages> pagesTable;

    @FXML
    private TableColumn<Pages, String> columnID;

    @FXML
    private TableColumn<Pages, String> columnURL;

    @FXML
    private TableColumn<Pages, String> columnSiteId;

    @FXML
    private TableColumn<Pages, String> columnFoundDate;

    @FXML
    private TableColumn<Pages, String> columnLastScan;

    @FXML
    private Button loadCSV_button;

    @FXML
    private ComboBox<Sites> checkSite;

    private StringBuffer getReq = null;
    JSONArray pagesArray = null;

    ObservableList<Pages> pagesData = FXCollections.observableArrayList();
    Date date=Calendar.getInstance().getTime();



    void initialize() {
        columnID.setCellValueFactory(new PropertyValueFactory<>("id"));
        columnURL.setCellValueFactory(new PropertyValueFactory<>("url"));
        columnSiteId.setCellValueFactory(new PropertyValueFactory<>("siteId"));
        columnFoundDate.setCellValueFactory(new PropertyValueFactory<>("foundDateTime"));
        columnLastScan.setCellValueFactory(new PropertyValueFactory<>("lastScanDate"));
        loadData();
        checkSite.setItems(sitesData);
        checkSite.setPromptText("Выберите сайт");
        loadPages("1");
        pagesTable.setItems(pagesData);

        checkSite.setCellFactory(p -> new ListCell<Sites>() {

            @Override
            protected void updateItem(Sites item, boolean empty) {
                super.updateItem(item, empty);
                if (item != null && !empty) {
                    setText(item.getName());
                } else {
                    setText(null);
                }
            }
        });
        checkSite.setButtonCell(new ListCell<Sites>() {
            @Override
            protected void updateItem(Sites item, boolean empty) {
                super.updateItem(item, empty);
                if (item != null && !empty) {
                    setText(item.getName());
                    siteLabel.setText(item.getName());
                    admLabel.setText("Данные на: "+ date);
//                    loadPages(item.getId());
//                    pagesTable.setItems(pagesData);
                    refresh(item.getId());
                } else {
                    setText(null);
                }
            }
        });
        loadCSV_button.setOnAction(event -> {
           try {
               writeCSV();
           } catch (Exception e) {
               e.printStackTrace();
           }
        });
    }

    public void loadPages(String idOfSite) {
        try {
            String myToken = URLEncoder.encode(Main.getToken());
            //   String sitesId = URLEncoder.encode(checkSite.getId());

            getReq = RestRequest.sendGet(RestRequest.PAGES + myToken + "&siteID=" + idOfSite, "GET");
            pagesArray = new JSONArray(getReq.toString());
            for (int i = 0; i < pagesArray.length(); i++) {
                JSONObject page = pagesArray.getJSONObject(i);
                String id = page.get("page_id").toString();
                String url = page.get("page_url").toString();
                String siteId = page.get("page_site_id").toString();
                String foundDate = page.get("page_found_date_str").toString();
                String lastScan = page.get("page_last_scan_date_str").toString();
                pagesData.add(new Pages(id, url, siteId, foundDate, lastScan));
            }
            pagesTable.setItems(pagesData);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void refresh(String id) {
        pagesData.clear();
        loadPages(id);
        pagesTable.setItems(pagesData);
    }

    public void writeCSV() throws Exception {
        Writer writer = null;
        try {
            File file = new File("AdminStat.csv.");
            writer = new BufferedWriter(new FileWriter(file));
            for (Pages pages : pagesData) {
                String row = pages.getId() + "," + pages.getUrl() + "," + pages.getSiteId()
                        + "," + pages.getFoundDateTime() + "," + pages.getLastScanDate() + "\n";
                writer.write(row);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            writer.flush();
            writer.close();
        }
    }
}

