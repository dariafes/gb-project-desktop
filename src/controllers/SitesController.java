package controllers;

import api.RestRequest;
import data.Sites;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URL;
import java.net.URLEncoder;
import java.util.Optional;
import java.util.ResourceBundle;

public class SitesController {
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TableView<Sites> sitesTable;

    @FXML
    private TableColumn<Sites, String> columnAddby;

    @FXML
    private TableColumn<Sites, String> columnId;

    @FXML
    private TableColumn<Sites, String> columnName;

    @FXML
    private TableColumn<Sites, String> columnSiteDescr;

    @FXML
    private Button add_button;

    @FXML
    private Button editSite_button;

    @FXML
    private Button delete_button;

    @FXML
    private Button clearSelection_button;

    @FXML
    private Button loadCSV_button;

    @FXML
    private TextField txt_name;

    @FXML
    private TextField txt_descr;

    @FXML
    private TextField txt_ID;

    @FXML
    private TextField search_field;

    private StringBuffer getRequest = null;
    private StringBuffer deleteRequest = null;
    private StringBuffer postRequest = null;
    private String postReq = null;
    private String patchRequest = null;
    JSONArray sitesArray = null;

    ObservableList<Sites> sitesData = FXCollections.observableArrayList();
    @FXML
    void initialize() {
        columnAddby.setCellValueFactory(new PropertyValueFactory<>("addby"));
        columnId.setCellValueFactory(new PropertyValueFactory<>("id"));
        columnName.setCellValueFactory(new PropertyValueFactory<>("name"));
        columnSiteDescr.setCellValueFactory(new PropertyValueFactory<>("siteDescription"));
        loadData();
        sitesTable.setItems(sitesData);
        sitesTable.setOnMouseClicked(event -> {
            selectRow();
        });
        clearSelection_button.setOnAction(event -> {
            sitesTable.getSelectionModel().clearSelection();
            clearFields();
        });
        add_button.setOnAction(event -> {
            if (validateFields()) {
                addNewSite();
                refreshTable();
            }
        });
        delete_button.setOnAction(event -> {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Удаление");
            alert.setHeaderText(null);
            alert.setContentText("Вы уверены, что хотите удалить сайт " + txt_name.getText() + " ?");
            alert.getDialogPane().setStyle("-fx-background-color:" +
                    " linear-gradient(from 0% 0% to 100% 80%, #45BCCD 0%, #9AD9E2 250%);");
            Optional<ButtonType> action = alert.showAndWait();
            if (action.get() == ButtonType.OK) {
                deleteSite();
                refreshTable();
            }
        });
        editSite_button.setOnAction(event -> {
            if (validateFields()) {
                patchSite();
                refreshTable();
            }
        });

    }
    public void loadData() {
        try {
            String myToken = URLEncoder.encode(Main.getToken());
            getRequest = RestRequest.sendGet(RestRequest.SITES + myToken,"GET");
        sitesArray = new JSONArray(getRequest.toString());
        for (int i = 0; i < sitesArray.length(); i++) {
            JSONObject site = sitesArray.getJSONObject(i);
            String addby = site.get("site_addby").toString();
            String id = site.get("site_id").toString();
            String name = site.get("site_name").toString();
            String siteDescription = site.get("site_siteDescription").toString();
            sitesData.add(new Sites(addby,id,name,siteDescription));
        } sitesTable.setItems(sitesData);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public String createNewSite(){
        JSONObject newSite = new JSONObject();
        newSite.put("token_auth",Main.getToken());
        newSite.put("description",txt_descr.getText());
        newSite.put("name",txt_name.getText());
        newSite.put("pageURL","https://" + txt_name.getText());
        String json = newSite.toString();
        return json;
    }
    public String selectSiteToDelete(){
        JSONObject selectedSite = new JSONObject();
        selectedSite.put("token_auth",Main.getToken());
        selectedSite.put("site_id",txt_ID.getText());
        String obj = selectedSite.toString();
        return obj;
    }
    public String selectSiteToPatch(){
        JSONObject patchSite = new JSONObject();
        patchSite.put("token_auth",Main.getToken());
        patchSite.put("site_id",txt_ID.getText());
        patchSite.put("description",txt_descr.getText());
        patchSite.put("name",txt_name.getText());
        String patched = patchSite.toString();
        return patched;
    }
    public void addNewSite(){
        try {
            postReq = RestRequest.methPost(RestRequest.SITES, createNewSite());
            clearFields();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void deleteSite(){
        try {
            deleteRequest = RestRequest.sendPost(RestRequest.SITES,
                    selectSiteToDelete(),"DELETE");
            clearFields();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void patchSite(){
        try {
            patchRequest = RestRequest.methPatch(RestRequest.SITES,
                    selectSiteToPatch());
            clearFields();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void selectRow(){
        Sites site = sitesTable.getSelectionModel().getSelectedItem();
        txt_ID.setText(site.getId());
        txt_name.setText(site.getName());
        txt_descr.setText(site.getSiteDescription());
    }
    public void clearFields() {
        txt_ID.clear();
        txt_name.clear();
        txt_descr.clear();
    }

    public void refreshTable() {
        sitesData.clear();
        loadData();
        sitesTable.setItems(sitesData);
    }
    private boolean validateFields() {
        if (txt_descr.getText().isEmpty() | txt_name.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Пустые Поля");
            alert.setHeaderText(null);
            alert.setContentText("Пожалуйста, заполните все доступные пустые поля.");
            alert.getDialogPane().setStyle("-fx-background-color:" +
                    " linear-gradient(from 0% 0% to 100% 80%, #45BCCD 0%, #9AD9E2 250%);");
            alert.showAndWait();
            return false;
        }
        return true;
    }
}
