package controllers;

import api.RestRequest;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.json.JSONObject;

public class Main extends Application {

    private static String token;
    private static String userID;
    private static String userLogin;
    public static int isStarted = 0;
    public static String ok = "1";

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("../fxmlFiles/auth.fxml"));
        primaryStage.setTitle("gb-project-desktop");
        primaryStage.setScene(new Scene(root, 671, 445));
        primaryStage.show();
        primaryStage.setResizable(false);
    }

    private static void keepConnectionAlive() throws InterruptedException {
        Runnable r = () -> {
            while (true) {
                try {
                    StringBuffer authKeepAlive = RestRequest.keepAlive(RestRequest.AUTH, token);
                    JSONObject keepAliveResult = new JSONObject(authKeepAlive.toString());
                    String success = keepAliveResult.get("success").toString();
                    if (success.equals(ok)) {
                        System.out.println(RestRequest.keepAlive(RestRequest.AUTH, token));
                    } else {
                        System.out.println("error");
                    }
                    Thread.sleep(50000);

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        };

        Thread keepAlive = new Thread(r);
        keepAlive.start();
        keepAlive.join();
    }

    public static void main(String[] args) {
        launch(args);
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                try {
                    launch(args);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        if (isStarted == 1) {
            try {
                keepConnectionAlive();
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * @return the token
     */
    public static String getToken() {
        return token;
    }

    /**
     * @return the userID
     */
    public static String getUserID() {
        return userID;
    }

    /**
     * @return the userLogin
     */
    public static String getUserLogin() {
        return userLogin;
    }

    /**
     * @param userID the userID to set
     */
    public static void setUserID(String userID) {
        Main.userID = userID;
    }

    /**
     * @param token the token to set
     */
    public static void setToken(String token) {
        Main.token = token;
    }

    /**
     * @param userLogin the userLogin to set
     */
    public static void setUserLogin(String userLogin) {
        Main.userLogin = userLogin;
    }


}
