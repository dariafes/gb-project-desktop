package controllers;

import api.RestRequest;
import data.Keywords;
import data.Persons;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URL;
import java.net.URLEncoder;
import java.util.Optional;
import java.util.ResourceBundle;

public class KeywordsController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TableView<Persons> personsTable;

    @FXML
    private TableColumn<Persons, String> columnAddby;

    @FXML
    private TableColumn<Persons, String> columnId;

    @FXML
    private TableColumn<Persons, String> columnName;

    @FXML
    private TableView<Keywords> keywordsTable;

    @FXML
    private TableColumn<Keywords, String> keywordIdCol;

    @FXML
    private TableColumn<Keywords, String> keywordNameCol;

    @FXML
    private Button add_button;

    @FXML
    private Button editPerson_button;

    @FXML
    private Button delete_button;

    @FXML
    private Button clearSelection_button;

    @FXML
    private Button loadCSV_button;

    @FXML
    private TextField txt_name;

    @FXML
    private TextField txt_ID;

    @FXML
    private TextField search_field;

    @FXML
    private Button showKeywords;

    private StringBuffer getRequest = null;
    private StringBuffer deleteRequest = null;
    private StringBuffer postRequest = null;
    private String postReq = null;
    private String patchRequest = null;
    private JSONArray personsArray = null;

    ObservableList<Persons> personsData = FXCollections.observableArrayList();
    ObservableList<Keywords> keywordsData = FXCollections.observableArrayList();

    @FXML
    void initialize() {
        columnAddby.setCellValueFactory(new PropertyValueFactory<>("addby"));
        columnId.setCellValueFactory(new PropertyValueFactory<>("id"));
        columnName.setCellValueFactory(new PropertyValueFactory<>("name"));
        keywordIdCol.setCellValueFactory(new PropertyValueFactory<>("keyword_id"));
        keywordNameCol.setCellValueFactory(new PropertyValueFactory<>("keyword_name"));
        loadData();
       // loadKeywords();
        personsTable.setItems(personsData);
        keywordsTable.setItems(keywordsData);
        personsTable.setOnMouseClicked(event -> {
            selectRow();
        });
        clearSelection_button.setOnAction(event -> {
            personsTable.getSelectionModel().clearSelection();
            clearFields();
        });
        add_button.setOnAction(event -> {
            if (validateFields()) {
                addNewPerson();
                refreshTable();
            }
        });
        delete_button.setOnAction(event -> {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Удаление");
            alert.setHeaderText(null);
            alert.setContentText("Вы уверены, что хотите удалить персону " + txt_name.getText() + " ?");
            alert.getDialogPane().setStyle("-fx-background-color:" +
                    " linear-gradient(from 0% 0% to 100% 80%, #45BCCD 0%, #9AD9E2 250%);");
            Optional<ButtonType> action = alert.showAndWait();
            if (action.get() == ButtonType.OK) {
                deletePerson();
                refreshTable();
            }
        });
        editPerson_button.setOnAction(event -> {
            if (validateFields()) {
                patchPerson();
                refreshTable();
            }
        });
        showKeywords.setOnAction(event -> {
        //    loadKeywords();
        });
    }
    public void loadData() {
        try {
            String myToken = URLEncoder.encode(Main.getToken());
            getRequest = RestRequest.sendGet(RestRequest.PERSONS+myToken,"GET");
        personsArray = new JSONArray(getRequest.toString());
        for (int i = 0; i < personsArray.length(); i++) {
            JSONObject person = personsArray.getJSONObject(i);
            String addby = person.get("person_addby").toString();
            String id = person.get("person_id").toString();
            String name = person.get("person_name").toString();
            personsData.add(new Persons(addby,id,name));
        } personsTable.setItems(personsData);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void loadKeywords() {
        try {
            String token = URLEncoder.encode(Main.getToken());
            String id = URLEncoder.encode(txt_ID.getText());
            // String req = RestRequest.methodGet(id,token);
            getRequest = RestRequest.sendGet(RestRequest.PERSONS1 + id + "?token_auth=" + token, "GET");
          //  JSONObject object = new JSONObject(getRequest.toString());
           // String addby = object.get("person_addby").toString();
           // String persid = object.get("person_id").toString();
            JSONArray arr = new JSONArray(getRequest.toString());
            for (int i = 0; i < arr.length(); i++) {
                JSONObject keyword = arr.getJSONObject(i);
                String myId = keyword.get("keyword_id").toString();
                String name = keyword.get("keyword_name").toString();
                keywordsData.add(new Keywords(myId,name));
            } keywordsTable.setItems(keywordsData);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public String createNewPerson(){
        JSONObject newPerson = new JSONObject();
        newPerson.put("token_auth",Main.getToken());
        newPerson.put("name",txt_name.getText());
        String json = newPerson.toString();
        return json;
    }
    public String selectPersonToDelete(){
        JSONObject selectedPerson = new JSONObject();
        selectedPerson.put("token_auth",Main.getToken());
        selectedPerson.put("person_id",txt_ID.getText());
        String obj = selectedPerson.toString();
        return obj;
    }
    public String selectPersonToPatch(){
        JSONObject patchPerson = new JSONObject();
        patchPerson.put("token_auth",Main.getToken());
        patchPerson.put("name",txt_name.getText());
        patchPerson.put("person_id",txt_ID.getText());
        String patched = patchPerson.toString();
        return patched;
    }
    public void addNewPerson(){
        try {
            postReq = RestRequest.methPost(RestRequest.PERSONS,createNewPerson());
            clearFields();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void deletePerson(){
        try {
            deleteRequest = RestRequest.sendPost(RestRequest.PERSONS,
                    selectPersonToDelete(),"DELETE");
            clearFields();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void patchPerson(){
        try {
            patchRequest = RestRequest.methPatch(RestRequest.PERSONS,
                    selectPersonToPatch());
            clearFields();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void selectRow(){
        Persons person = personsTable.getSelectionModel().getSelectedItem();
        txt_ID.setText(person.getId());
        txt_name.setText(person.getName());
    }
    public void clearFields() {
        txt_ID.clear();
        txt_name.clear();
    }
    public void refreshTable() {
        personsData.clear();
        loadData();
        personsTable.setItems(personsData);
    }
    private boolean validateFields() {
        if (txt_name.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Пустое Поле");
            alert.setHeaderText(null);
            alert.setContentText("Пожалуйста, заполните все доступные пустые поля.");
            alert.getDialogPane().setStyle("-fx-background-color:" +
                    " linear-gradient(from 0% 0% to 100% 80%, #45BCCD 0%, #9AD9E2 250%);");
            alert.showAndWait();
            return false;
        }
        return true;
    }
}
