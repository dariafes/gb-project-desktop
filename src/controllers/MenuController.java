package controllers;

import animations.Shake;
import api.RestRequest;
import data.DailyStatPersonsSites;
import data.Logs;
import data.Sites;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.apache.commons.logging.Log;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MenuController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Label adminLabel;

    @FXML
    private BorderPane borderPane;

    @FXML
    private VBox adminMode;

    @FXML
    private Button menuLogOutButton;

    @FXML
    private Button menuAdminStatsButton;

    @FXML
    private Button menuUsersButton;

    @FXML
    private Button menuSitesButton;

    @FXML
    private Button menuKeywordsButton;

    @FXML
    private Button menuChangeToUserButton;

    @FXML
    private VBox userMode;

    @FXML
    private Button menuLogOutButton1;

    @FXML
    private Button menuTotalStatButton;

    @FXML
    private Button menuDailyStatButton;

    @FXML
    private Button menuBackToAdminButton;

    @FXML
    public TableView<Logs> logTable;

    @FXML
    private TableColumn<Logs, String> loginCol;

    @FXML
    private TableColumn<Logs, String> actCol;

    @FXML
    private TableColumn<Logs, String> dateCol;

    @FXML
    void adminSiteStats(MouseEvent event) { loadMenuPage("/fxmlFiles/adminSiteStats"); }

    @FXML
    void keywords(MouseEvent event) { loadMenuPage("/fxmlFiles/keywords"); }

    @FXML
    void sites(MouseEvent event) { loadMenuPage("/fxmlFiles/sites"); }

    @FXML
    void users(MouseEvent event) { loadMenuPage("/fxmlFiles/users"); }

    @FXML
    void totalStat(MouseEvent event) { loadMenuPage("/fxmlFiles/totalStat"); }

    @FXML
    void dailyStat(MouseEvent event) { loadMenuPage("/fxmlFiles/dailyStat");}

    @FXML
    void shakeAnim(MouseEvent event) {
        Shake shake = new Shake(menuAdminStatsButton);
        shake.playAnim();
    }
    @FXML
    void shakeAnim1(MouseEvent event) {
        Shake shake1 = new Shake(menuUsersButton);
        shake1.playAnim();
    }

    @FXML
    void shakeAnim2(MouseEvent event) {
        Shake shake2 = new Shake(menuSitesButton);
        shake2.playAnim();
    }

    @FXML
    void shakeAnim3(MouseEvent event) {
        Shake shake3 = new Shake(menuKeywordsButton);
        shake3.playAnim();
    }
    @FXML
    void shakeAnim4(MouseEvent event) {
        Shake shake4 = new Shake(menuTotalStatButton);
        shake4.playAnim();
    }

    @FXML
    void shakeAnim5(MouseEvent event) {
        Shake shake5 = new Shake(menuDailyStatButton);
        shake5.playAnim();
    }

    private StringBuffer deleteRequest = null;
    private String rq = null;
    private StringBuffer logsReq = null;
    JSONArray logsArray = null;
    public ObservableList<Logs> logsData = FXCollections.observableArrayList();
    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
    Date date=Calendar.getInstance().getTime();


    @FXML
    void initialize() {
        loadMenuPage("/fxmlFiles/users");
        adminLabel.setText(Main.getUserLogin());
        loadLogs();
        logTable.setItems(logsData);
        menuChangeToUserButton.setOnAction(event -> {
            changeMode(adminMode, userMode);
        });
        menuBackToAdminButton.setOnAction(event -> {
            changeMode(userMode, adminMode);
        });
        menuLogOutButton.setOnAction(event -> {
            deleteToken(menuLogOutButton);
        });
        menuLogOutButton1.setOnAction(event -> {
            deleteToken(menuLogOutButton1);
        });
        loginCol.setCellValueFactory(new PropertyValueFactory<>("login"));
        actCol.setCellValueFactory(new PropertyValueFactory<>("action"));
        dateCol.setCellValueFactory(new PropertyValueFactory<>("date"));
    }
    private void loadMenuPage(String fxmlPage) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource(fxmlPage + ".fxml"));
        } catch (IOException ex) {
            Logger.getLogger(MenuController.class.getName()).log(Level.SEVERE, null, ex);
        }
        borderPane.setCenter(root);
    }
    private void changeMode (VBox from, VBox to) {
        from.setVisible(false);
        from.setManaged(false);

        to.setVisible(true);
        to.setManaged(true);
    }

    public String logOut(){
        JSONObject selectedToken = new JSONObject();
        selectedToken.put("token_auth",Main.getToken());
        String obj = selectedToken.toString();
        return obj;
    }
    public void deleteToken(Button button){
        try {
            AuthController authController = new AuthController();
            deleteRequest = RestRequest.sendPost(RestRequest.AUTH,
                    logOut(),"DELETE");
            openNewScene(button,"/fxmlFiles/auth.fxml");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void openNewScene(Button button, String window) {
        button.getScene().getWindow().hide();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource(window));
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Parent root = loader.getRoot();
        Stage stage = new Stage();
        stage.setResizable(false);
        stage.setScene(new Scene(root));
        stage.showAndWait();
    }
    public void loadLogs() {
        try {
//            rq = RestRequest.methGetLogs();
            logsReq = RestRequest.sendGet("https://apismall.dtgb.solutions/v1/logs","GET");
            logsArray = new JSONArray(logsReq.toString());
            for (int i = 0; i < logsArray.length(); i++) {
                JSONObject arr = logsArray.getJSONObject(i);
                String login = arr.get("log_admin_id").toString();
                String action = arr.get("log_action").toString();
                String date = arr.get("log_date").toString();
                logsData.add(new Logs(login,action,date));
            } logTable.setItems(logsData);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
