package data;

import javafx.beans.property.SimpleStringProperty;

public class Pages {
    private final SimpleStringProperty id;
    private final SimpleStringProperty url;
    private final SimpleStringProperty siteId;
    private final SimpleStringProperty foundDateTime;
    private final SimpleStringProperty lastScanDate;

    public Pages(String idStr, String urlStr, String siteIdStr, String foundDateStr, String lastscanStr) {
        this.id = new SimpleStringProperty(idStr);
        this.url = new SimpleStringProperty(urlStr);
        this.siteId = new SimpleStringProperty(siteIdStr);
        this.foundDateTime = new SimpleStringProperty(foundDateStr);
        this.lastScanDate = new SimpleStringProperty(lastscanStr);
    }

    public String getId() {
        return id.get();
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public String getUrl() {
        return url.get();
    }

    public void setUrl(String url) {
        this.url.set(url);
    }

    public String getSiteId() {
        return siteId.get();
    }

    public void setSiteId(String siteId) {
        this.siteId.set(siteId);
    }

    public String getFoundDateTime() {
        return foundDateTime.get();
    }

    public void setFoundDateTime(String foundDateTime) {
        this.foundDateTime.set(foundDateTime);
    }

    public String getLastScanDate() {
        return lastScanDate.get();
    }

    public void setLastScanDate(String lastScanDate) {
        this.lastScanDate.set(lastScanDate);
    }
}
