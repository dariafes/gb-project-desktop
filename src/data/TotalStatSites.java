package data;

import javafx.beans.property.SimpleStringProperty;

public class TotalStatSites {
    private final SimpleStringProperty personId;
    private final SimpleStringProperty personName;
    private final SimpleStringProperty persRank;

    public TotalStatSites(String personIdStr, String personNameStr, String persRankStr) {
        this.personId = new SimpleStringProperty(personIdStr);
        this.personName = new SimpleStringProperty(personNameStr);
        this.persRank = new SimpleStringProperty(persRankStr);
    }

    public String getPersonId() {
        return personId.get();
    }

    public void setPersonId(String personId) {
        this.personId.set(personId);
    }

    public String getPersonName() {
        return personName.get();
    }

    public void setPersonName(String personName) {
        this.personName.set(personName);
    }

    public String getPersRank() {
        return persRank.get();
    }

    public void setPersRank(String persRank) {
        this.persRank.set(persRank);
    }
}
