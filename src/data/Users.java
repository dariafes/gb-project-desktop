package data;

import javafx.beans.property.SimpleStringProperty;

public class Users {
    private final SimpleStringProperty addby;
    private final SimpleStringProperty email;
    private final SimpleStringProperty id;
    private final SimpleStringProperty isadmin;
    private final SimpleStringProperty login;
    private final SimpleStringProperty password;

    public Users(String addbyStr, String emailStr, String idStr, String isadminStr, String loginStr, String passwordStr) {
        this.addby = new SimpleStringProperty(addbyStr);
        this.email = new SimpleStringProperty(emailStr);
        this.id = new SimpleStringProperty(idStr);
        this.isadmin = new SimpleStringProperty(isadminStr);
        this.login = new SimpleStringProperty(loginStr);
        this.password = new SimpleStringProperty(passwordStr);
    }

    public String getAddby() {
        return addby.get();
    }

    public void setAddby(String addby) {
        this.addby.set(addby);
    }


    public String getEmail() {
        return email.get();
    }

    public void setEmail(String email) {
        this.email.set(email);
    }


    public String getId() {
        return id.get();
    }

    public void setId(String id) {
        this.id.set(id);
    }


    public String getIsadmin() {
        return isadmin.get();
    }

    public void setIsadmin(String isadmin) {
        this.isadmin.set(isadmin);
    }


    public String getLogin() {
        return login.get();
    }

    public void setLogin(String login) {
        this.login.set(login);
    }

    public String getPassword() { return password.get(); }

    public void setPassword(String password) { this.password.set(password); }
}
