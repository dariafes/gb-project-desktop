package data;

import javafx.beans.property.SimpleStringProperty;

public class Sites {
    private final SimpleStringProperty addby;
    private final SimpleStringProperty id;
    private final SimpleStringProperty name;
    private final SimpleStringProperty siteDescription;

    public Sites(String addbyStr, String idStr, String nameStr, String siteDescriptionStr) {
        this.addby = new SimpleStringProperty(addbyStr);
        this.id = new SimpleStringProperty(idStr);
        this.name = new SimpleStringProperty(nameStr);
        this.siteDescription = new SimpleStringProperty(siteDescriptionStr);
    }

    public String getAddby() {
        return addby.get();
    }

    public SimpleStringProperty addbyProperty() {
        return addby;
    }

    public void setAddby(String addby) {
        this.addby.set(addby);
    }

    public String getId() {
        return id.get();
    }

    public SimpleStringProperty idProperty() {
        return id;
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getSiteDescription() {
        return siteDescription.get();
    }

    public SimpleStringProperty siteDescriptionProperty() {
        return siteDescription;
    }

    public void setSiteDescription(String siteDescription) {
        this.siteDescription.set(siteDescription);
    }
}
