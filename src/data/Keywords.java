package data;

import javafx.beans.property.SimpleStringProperty;

public class Keywords {
    private final SimpleStringProperty keyword_id;
    private final SimpleStringProperty keyword_name;

    public Keywords (String keywordId, String keywordName) {
        this.keyword_id = new SimpleStringProperty(keywordId);
        this.keyword_name = new SimpleStringProperty(keywordName);
    }

    public String getKeyword_id() { return keyword_id.get(); }

    public void setKeyword_id(String keyword_id) { this.keyword_id.set(keyword_id); }

    public String getKeyword_name() { return keyword_name.get(); }

    public void setKeyword_name(String keyword_name) { this.keyword_name.set(keyword_name); }
}
