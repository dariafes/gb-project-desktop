package data;

import javafx.beans.property.SimpleStringProperty;

public class Persons {
    private final SimpleStringProperty addby;
    private final SimpleStringProperty id;
    private final SimpleStringProperty name;

    public Persons(String addbyStr, String idStr, String nameStr) {
        this.addby = new SimpleStringProperty(addbyStr);
        this.id = new SimpleStringProperty(idStr);
        this.name = new SimpleStringProperty(nameStr);
    }

    public String getAddby() {
        return addby.get();
    }

    public SimpleStringProperty addbyProperty() {
        return addby;
    }

    public void setAddby(String addby) {
        this.addby.set(addby);
    }

    public String getId() {
        return id.get();
    }

    public SimpleStringProperty idProperty() {
        return id;
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }
}
