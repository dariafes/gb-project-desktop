package data;

import javafx.beans.property.SimpleStringProperty;

public class TotalStatPersons {
    private final SimpleStringProperty siteId;
    private final SimpleStringProperty siteName;
    private final SimpleStringProperty personRank;

    public TotalStatPersons(String siteIdStr, String siteNameStr, String personRankStr) {
        this.siteId = new SimpleStringProperty(siteIdStr);
        this.siteName = new SimpleStringProperty(siteNameStr);
        this.personRank = new SimpleStringProperty(personRankStr);
    }

    public String getSiteId() {
        return siteId.get();
    }

    public void setSiteId(String siteId) {
        this.siteId.set(siteId);
    }

    public String getSiteName() {
        return siteName.get();
    }

    public void setSiteName(String siteName) {
        this.siteName.set(siteName);
    }

    public String getPersonRank() {
        return personRank.get();
    }

    public void setPersonRank(String personRank) {
        this.personRank.set(personRank);
    }
}
