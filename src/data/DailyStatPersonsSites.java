package data;

import javafx.beans.property.SimpleStringProperty;

public class DailyStatPersonsSites {
    private final SimpleStringProperty rankDate;
    private final SimpleStringProperty personRank;

    public DailyStatPersonsSites(String rankDateStr, String personRankStr) {
        this.rankDate = new SimpleStringProperty(rankDateStr);
        this.personRank = new SimpleStringProperty(personRankStr);
    }

    public String getRankDate() {
        return rankDate.get();
    }

    public void setRankDate(String rankDate) {
        this.rankDate.set(rankDate);
    }

    public String getPersonRank() {
        return personRank.get();
    }

    public void setPersonRank(String personRank) {
        this.personRank.set(personRank);
    }
}
