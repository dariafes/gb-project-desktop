package data;

import javafx.beans.property.SimpleStringProperty;

public class Logs {
    private final SimpleStringProperty login;
    private final SimpleStringProperty action;
    private final SimpleStringProperty date;

    public Logs(String loginStr, String actionStr, String dateStr){
        this.login = new SimpleStringProperty(loginStr);
        this.action = new SimpleStringProperty(actionStr);
        this.date = new SimpleStringProperty(dateStr);
    }

    public String getLogin() {
        return login.get();
    }

    public void setLogin(String login) {
        this.login.set(login);
    }

    public String getAction() {
        return action.get();
    }

    public void setAction(String action) {
        this.action.set(action);
    }

    public String getDate() {
        return date.get();
    }

    public void setDate(String date) {
        this.date.set(date);
    }
}
