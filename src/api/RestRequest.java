package api;

import controllers.Main;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.*;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;

import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.*;
import java.net.*;


public class RestRequest {

    private static final String USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:60.0) Gecko/20100101 Firefox/60.0";
    private static final String HOST = "https://smallapi.dtgb.solutions/v1/";
    public static final String AUTH = HOST + "auth";
    public static final String USERS = HOST + "users?token_auth=";
    public static final String USERS_WITHOUT_TOKEN = HOST + "users";
    public static final String PERSONS = HOST + "persons?token_auth=";
    public static final String PERSONS1 = HOST + "persons/";
    public static final String SITES = HOST + "sites?token_auth=";
    public static final String SITES1 = HOST + "sites";
    public static final String PAGES = HOST + "pages?token_auth=";


    public static StringBuffer authRequest(String url, String login, String password) throws Exception {
        DataOutputStream wr;
        int responseCode;
        BufferedReader in;
        String inputLine;
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        StringBuffer response = new StringBuffer();
        String urlParameters = "{\"user\":\"" + login + "\",\"password\":\"" + password + "\"}";

        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Content-Type", "application/json");

        con.setDoOutput(true);

        wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();

        responseCode = con.getResponseCode();

        in = new BufferedReader(new InputStreamReader(con.getInputStream()));

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return response;
    }

    public static StringBuffer sendGet(String url, String method) throws Exception {

        int responseCode;
        BufferedReader in;
        String inputLine;
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        StringBuffer response = new StringBuffer();

        // GET options
        con.setRequestMethod(method);
        con.setRequestProperty("User-Agent", USER_AGENT);

        responseCode = con.getResponseCode();

        in = new BufferedReader(new InputStreamReader(con.getInputStream()));

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return response;
    }

    public static StringBuffer sendPost(String url, String urlParameters, String method) throws Exception {
        URL obj = new URL(url);

        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        DataOutputStream wr;
        int responseCode;
        BufferedReader in;
        String inputLine;
        StringBuffer response = new StringBuffer();

        con.setRequestMethod(method);
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Content-Type", "application/json");
//		con.addRequestProperty("user", "Admin1");
//		con.addRequestProperty("password", "1111");

        con.setDoOutput(true);

        wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();

        responseCode = con.getResponseCode();

        in = new BufferedReader(new InputStreamReader(con.getInputStream()));

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return response;
    }
    public static String methPost(String url, String urlParameters) throws Exception {

        HttpClient httpClient = new DefaultHttpClient();
        String responseStr = "";
        try {
            HttpPost httpPost = new HttpPost(url);
            StringEntity params =new StringEntity(urlParameters, "UTF-8");
            httpPost.addHeader("content-type", "application/json");
            httpPost.setEntity(params);
            HttpResponse response = httpClient.execute(httpPost);
            HttpEntity entity = response.getEntity();
            responseStr = EntityUtils.toString(entity, "UTF-8");

        }catch (Exception ex) {
            ex.printStackTrace();
            // handle exception here
        } finally {
            httpClient.getConnectionManager().shutdown();
        }
        return responseStr;
    }
    public static String methDelete(String pathName, String param, String paramValue) throws Exception {

        HttpClient httpClient = new DefaultHttpClient();
        String responseStr = "";
        URI uri = new URIBuilder()
                .setScheme("https")
                .setHost("localhost:5050/v1")
                .setPath(pathName)
                .setParameter("token_auth",Main.getToken())
                .setParameter(param,paramValue)
                .build();
        try {
            HttpDelete httpDelete = new HttpDelete(uri);
            httpDelete.addHeader("content-type", "application/json");
            HttpResponse response = httpClient.execute(httpDelete);

        }catch (Exception ex) {
            ex.printStackTrace();
            // handle exception here
        } finally {
            httpClient.getConnectionManager().shutdown();
        }
        return responseStr;
    }
    public static String methPatch(String url, String urlParameters) throws Exception {

        HttpClient httpClient = new DefaultHttpClient();
        String responseString = "";
        try {
            HttpPatch request = new HttpPatch(url);
            StringEntity params =new StringEntity(urlParameters, "UTF-8");
            request.addHeader("content-type", "application/json");
            request.setEntity(params);
            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            responseString = EntityUtils.toString(entity, "UTF-8");

        }catch (Exception ex) {
            ex.printStackTrace();
            // handle exception here
        } finally {
            httpClient.getConnectionManager().shutdown();
        }
        return responseString;
    }

    public static String methGetDaily(String paramFrom, String paramTill, String paramPersonId, String paramSite,
                                 String paramGroupDate) throws URISyntaxException,IOException {
        String result = "";

        URI uri = new URIBuilder()
                .setScheme("https")
                .setHost("apismall.dtgb.solutions/v1")
                .setPath("/rating")
                .setParameter("_from", paramFrom)
                .setParameter("_till", paramTill)
                .setParameter("_personids", paramPersonId)
                .setParameter("_sites", paramSite)
                .setParameter("_groupdate",paramGroupDate)
                .build();
        try {
            CloseableHttpClient httpclient = HttpClients.createDefault();
            HttpGet httpget = new HttpGet(uri);
            httpget.addHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
            CloseableHttpResponse response = httpclient.execute(httpget);

            try {
                result = EntityUtils.toString(response.getEntity(),"UTF-8");
            } finally {
                response.close();
            }
        } catch (Exception e) {
          e.printStackTrace();
        }
        return result;
    }
    public static String methGetToTal(String paramFrom, String paramTill, String paramSiteOrPerson, String param,
                                 String paramGroupDate) throws URISyntaxException,IOException {
        String res = "";

        URI uri = new URIBuilder()
                .setScheme("https")
                .setHost("apismall.dtgb.solutions/v1")
                .setPath("/rating")
                .setParameter("_from", paramFrom)
                .setParameter("_till", paramTill)
                .setParameter(paramSiteOrPerson, param)
                .setParameter("_groupdate",paramGroupDate)
                .build();
        try {
            CloseableHttpClient httpclient = HttpClients.createDefault();
            HttpGet httpget = new HttpGet(uri);
            httpget.addHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
            CloseableHttpResponse response = httpclient.execute(httpget);

            try {
                res = EntityUtils.toString(response.getEntity(),"UTF-8");
            } finally {
                response.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }
//    public static String methGetLogs() throws URISyntaxException,IOException {
//        String res = "";
//
//        URI uri = new URIBuilder()
//                .setScheme("https")
//                .setHost("apismall.dtgb.solutions/v1")
//                .setPath("/logs")
//                .build();
//        try {
//            CloseableHttpClient httpclient = HttpClients.createDefault();
//            HttpGet httpget = new HttpGet(uri);
//            httpget.addHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
//            CloseableHttpResponse response = httpclient.execute(httpget);
//
//            try {
//                res = EntityUtils.toString(response.getEntity(),"UTF-8");
//            } finally {
//                response.close();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return res;
//    }



    public static StringBuffer keepAlive(String url, String token) throws Exception {
        DataOutputStream wr;
        int responseCode;
        BufferedReader in;
        String inputLine;
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        StringBuffer response = new StringBuffer();
        String urlParameters = "{\"token_auth\":\"" + token + "\"}";

        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Content-Type", "application/json");

        con.setDoOutput(true);

        wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();

        responseCode = con.getResponseCode();

        in = new BufferedReader(new InputStreamReader(con.getInputStream()));

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return response;
    }


}
